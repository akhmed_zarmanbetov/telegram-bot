import os


TOKEN = os.environ['TELEGRAM_TOKEN']
ADD_BUTTON = 'Добавить дело'
SHOW_BUTTON = 'Показать дела'
DELETE_BUTTON = 'Удалить дело'
DELETE_ALL_BUTTON = 'Удалить все дела'

DB_NAME = 'planner.db'
