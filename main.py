import telebot
from telebot import types

from telegram_bot import config as cfg, exceptions as ex
from telegram_bot.database import Planner


bot = telebot.TeleBot(token=cfg.TOKEN)
planner = Planner()


@bot.message_handler(commands=['start'])
def send_keyboard(message, text="Привет, чем я могу помочь?"):
    keyboard = types.ReplyKeyboardMarkup()
    add_button = types.KeyboardButton(cfg.ADD_BUTTON)
    show_button = types.KeyboardButton(cfg.SHOW_BUTTON)
    delete_button = types.KeyboardButton(cfg.DELETE_BUTTON)
    delete_all_button = types.KeyboardButton(cfg.DELETE_ALL_BUTTON)
    keyboard.row(add_button, show_button)
    keyboard.row(delete_button, delete_all_button)

    message = bot.send_message(
        chat_id=message.chat.id,
        text=text,
        reply_markup=keyboard,
    )

    bot.register_next_step_handler(message, callback)


@bot.message_handler(content_types=['text'])
def bad_text(message):
    send_keyboard(
        message=message,
        text='Я тебя не понимаю, пользуйся кнопками!',
    )


def callback(message):
    if message.text == cfg.ADD_BUTTON:
        msg = bot.send_message(
            chat_id=message.chat.id,
            text='Давайте добавим дело. Напишите его в чат.'
        )
        bot.register_next_step_handler(msg, add)

    elif message.text == cfg.SHOW_BUTTON:
        show(message=message)
    elif message.text == cfg.DELETE_BUTTON:
        try:
            tasks = planner.get_tasks(message=message)
            keyboard = types.ReplyKeyboardMarkup(row_width=2)
            for task in tasks:
                task_button = types.KeyboardButton(task)
                keyboard.add(task_button)

            msg = bot.send_message(
                chat_id=message.chat.id,
                text="Выбери задачу для удаления:",
                reply_markup=keyboard,
            )

            bot.register_next_step_handler(msg, delete)
        except ex.TasksNotExists:
            bot.send_message(
                chat_id=message.chat.id,
                text='У тебя еще нет задач!'
            )

    elif message.text == cfg.DELETE_ALL_BUTTON:
        delete_all(message=message)

    else:
        send_keyboard(
            message=message,
            text='Я тебя не понимаю, пользуйся кнопками!',
        )


def add(message):
    planner.add(message=message)
    bot.send_message(
        chat_id=message.chat.id,
        text="Дело добавлено!"
    )
    send_keyboard(message=message, text="Чем еще могу помочь?")


def show(message):
    try:
        tasks: str = planner.show(message=message)
        bot.send_message(
            chat_id=message.chat.id,
            text=tasks,
        )
    except ex.TasksNotExists:
        bot.send_message(
            chat_id=message.chat.id,
            text='У тебя еще нет задач!'
        )
    finally:
        send_keyboard(message=message, text="Чем еще могу помочь?")


def delete(message):
    planner.delete(message=message)
    bot.send_message(
        chat_id=message.chat.id,
        text=f"Задача: {message.text} удалена.",
    )
    send_keyboard(message=message, text="Чем еще могу помочь?")


def delete_all(message):
    planner.delete_all(message=message)
    bot.send_message(
        chat_id=message.chat.id,
        text="Все задачи удалены.",
    )
    send_keyboard(message=message, text="Чем еще могу помочь?")


if __name__ == '__main__':
    bot.infinity_polling()
